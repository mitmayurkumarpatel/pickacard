/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.util.Scanner;
/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        
        Scanner sc = new Scanner(System.in);
        Card[] magicHand = new Card[7];
       
        for (int i=0; i<magicHand.length; i++)
        {
              System.out.println("Select any card");
              String random = sc.next();
            Card c = new Card();
            if(random.equals("Hearts"))
            {
                System.out.println("You have choosen magical cards");                     
            }
            else if(random.equals("Diamonds"))
            {
                System.out.println("You have choosen magical cards");                     
            }
            else if(random.equals("Spades"))
            {
                System.out.println("You have choosen magical cards");                     
            }
            else if(random.equals("Clubs"))
            {
                System.out.println("You have choosen magical cards");                     
            }
           
            else
            {
                 System.out.println("You have choosen simple cards"); 
                 break;
            }
            //c.setValue(insert call to random number generator here)
            //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
        }
        
        //insert code to ask the user for Card value and suit, create their card
        // and search magicHand here
        //Then report the result here
    }
    
}
